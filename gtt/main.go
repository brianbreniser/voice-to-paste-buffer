package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strings"
	"sync"
)

type Config struct {
	Debug              bool
	Keep               bool
	Local              bool
	ModsRole           string
	UseGrammar         bool
	WhisperModel       string
	ModsAvailable      bool
	PrependMods        bool
	LocalLanguageModel string
	CliWhisper         bool
}

const (
	localEndpoint       = "http://0.0.0.0:8080/v1/audio/transcriptions"
	remoteEndpoint      = "https://api.openai.com/v1/audio/transcriptions"
	localWhisperCommand = "whisper --model small.en --language en --fp16 False recording.wav"
	wavFile             = "recording.wav"
	mp3File             = "recording.mp3"
)

func debug(config Config, msg string) {
	if config.Debug {
		log.Printf("[DEBUG] %s", msg)
	}
}

func main() {
	config := parseFlags()
	debug(config, "Starting application")

	if err := checkDependencies(&config); err != nil {
		log.Fatal(err)
	}
	debug(config, "Dependencies checked")

	shouldRecord := true
	if err := checkExistingFiles(); err != nil {
		debug(config, "Found existing files")
		fmt.Print("Audio files exist. Use existing files instead of recording new ones? (y/n): ")
		var response string
		fmt.Scanln(&response)

		switch response {
		case "y", "Y", "yes", "YES":
			debug(config, "Using existing audio files")
			fmt.Println("Using existing audio files")
			shouldRecord = false
		default:
			debug(config, "Cleaning up existing files")
			cleanup()
		}
	}

	if shouldRecord {
		debug(config, "Starting audio recording")
		if err := recordAudio(config); err != nil {
			log.Fatal(err)
		}

		if !config.CliWhisper { // We can use the cli with the wav file from above
			debug(config, "Converting to MP3")
			if err := convertToMP3(); err != nil {
				log.Fatal(err)
			}
		}
	}

	debug(config, "Starting transcription")
	text, err := transcribeAudio(config)
	if err != nil {
		log.Fatal(err)
	}

	debug(config, fmt.Sprintf("Raw response: %s", text))

	if config.CliWhisper {
		text = trimResponseForCliWhisper(config, text)
	} else if config.Local {
		text = trimResponseForLocalWhisper(text)
	} else {
		text = trimResponseForOpenAIWhisper(text)
	}

	processedText, err := processResponse(text, config)
	if err != nil {
		log.Fatal(err)
	}

	debug(config, "Copying to clipboard")
	if err := copyToClipboard(processedText); err != nil {
		log.Fatal(err)
	}

	if !config.Keep {
		debug(config, "Cleaning up files")
		cleanup()
	}

	debug(config, "Operation complete")
	fmt.Println("done")
}

func parseFlags() Config {
	fmt.Println("Parsing flags")
	config := Config{}
	flag.BoolVar(&config.Debug, "d", false, "Enable debug output")
	flag.BoolVar(&config.Debug, "debug", false, "Enable debug output")
	flag.BoolVar(&config.Keep, "k", false, "Keep audio files")
	flag.BoolVar(&config.Keep, "keep", false, "Keep audio files")
	flag.BoolVar(&config.Local, "l", false, "Use local API endpoint")
	flag.BoolVar(&config.Local, "local", false, "Use local API endpoint")
	flag.BoolVar(&config.PrependMods, "m", false, "Prepend the mods string and role to the beginning of the response")
	flag.BoolVar(&config.PrependMods, "mods", false, "Prepend the mods string and role to the beginning of the response")
	flag.StringVar(&config.ModsRole, "r", "", "Mods role to use")
	flag.StringVar(&config.ModsRole, "role", "", "Mods role to use")
	flag.StringVar(&config.WhisperModel, "w", "whisper-1", "Whisper model to use")
	flag.StringVar(&config.WhisperModel, "whispermodel", "whisper-1", "Whisper model to use")
	flag.StringVar(&config.LocalLanguageModel, "lm", "dm", "Local model to use")
	flag.BoolVar(&config.CliWhisper, "c", false, "Use the whisper command line tool")
	flag.BoolVar(&config.CliWhisper, "cliwhisper", false, "Use the whisper command line tool")
	flag.Parse()

	if config.CliWhisper {
		config.Local = true
	}

	return config
}

func checkDependencies(config *Config) error {
	debug(*config, "Checking dependencies")
	deps := []string{"arecord", "lame", "wl-copy"}
	for _, dep := range deps {
		if _, err := exec.LookPath(dep); err != nil {
			return fmt.Errorf("%s is not installed", dep)
		}
		debug(*config, fmt.Sprintf("Found dependency: %s", dep))
	}

	if config.CliWhisper {
		if _, err := exec.LookPath("whisper"); err != nil {
			return fmt.Errorf("whisper command not found but -c/-cliwhisper flag was specified")
		}
		debug(*config, "Found whisper command")
	}

	if _, err := exec.LookPath("mods"); err != nil {
		debug(*config, "Mods not found")
		config.ModsAvailable = false
		fmt.Println("Note: 'mods' command not found. Mods-related features will be unavailable.")
	} else {
		debug(*config, "Mods available")
		config.ModsAvailable = true
	}

	if config.ModsRole != "" && !config.ModsAvailable {
		return fmt.Errorf("mods role specified but 'mods' command is not installed")
	}

	return nil
}

func checkExistingFiles() error {
	files := []string{wavFile, mp3File}
	for _, file := range files {
		if _, err := os.Stat(file); err == nil {
			return fmt.Errorf("%s already exists", file)
		}
	}
	return nil
}

func recordAudio(config Config) error {
	debug(config, "Recording audio")
	fmt.Println("Recording audio...")
	fmt.Println("Press Enter to stop recording or Ctrl+C to stop and process")

	debug(config, "Setting up arecord")
	cmd := exec.Command("arecord", "-f", "cd", wavFile)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	// Setup signal handling
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)

	// Use wait group to coordinate between input and signal handlers
	var wg sync.WaitGroup
	wg.Add(1)

	debug(config, "Starting recording")
	err := cmd.Start()
	if err != nil {
		return err
	}

	// Handle Ctrl+C
	go func() {
		<-sigChan
		debug(config, "Caught interrupt signal")
		cmd.Process.Signal(os.Interrupt)
		wg.Done()
	}()

	// Handle Enter key
	go func() {
		fmt.Scanln()
		debug(config, "Enter key pressed")
		cmd.Process.Signal(os.Interrupt)
		wg.Done()
	}()

	// Wait for either signal
	wg.Wait()

	debug(config, "Waiting for recording to finish")
	if err := cmd.Wait(); err != nil {
		if err.Error() != "signal: interrupt" && err.Error() != "exit status 1" {
			return fmt.Errorf("failed to stop recording: %v", err)
		}
	}

	return nil
}

func convertToMP3() error {
	cmd := exec.Command("lame", wavFile, mp3File)
	return cmd.Run()
}

func transcribeAudioWithLocalWhisperCli(config Config) (string, error) {
	debug(config, "Starting transcription with whisper CLI")

	fmt.Println("Using whisper CLI")
	cmd := exec.Command("sh", "-c", localWhisperCommand)
	output, err := cmd.Output()
	if err != nil {
		return "", fmt.Errorf("whisper CLI failed: %v", err)
	}

	return string(output), nil
}

func transcribeAudio(config Config) (string, error) {
	debug(config, "Starting transcription")

	if config.CliWhisper {
		return transcribeAudioWithLocalWhisperCli(config)
	}

	endpoint := remoteEndpoint
	if config.Local {
		debug(config, "Using local endpoint")
		endpoint = localEndpoint
	}

	file, err := os.Open(mp3File)
	if err != nil {
		return "", err
	}
	defer file.Close()

	debug(config, "Preparing multipart request")
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	part, err := writer.CreateFormFile("file", filepath.Base(mp3File))
	if err != nil {
		return "", err
	}
	io.Copy(part, file)

	writer.WriteField("model", config.WhisperModel)
	writer.Close()

	debug(config, "Creating HTTP request")
	req, err := http.NewRequest("POST", endpoint, body)
	if err != nil {
		return "", err
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())
	if !config.Local {
		debug(config, "Setting OpenAI API key")
		apiKey := os.Getenv("OPENAI_API_KEY")
		if apiKey == "" {
			return "", fmt.Errorf("OPENAI_API_KEY not set")
		}
		req.Header.Set("Authorization", "Bearer "+apiKey)
	}

	debug(config, "Sending request")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(responseBody), nil
}

func trimResponseForOpenAIWhisper(text string) string {
	text = strings.TrimSpace(text)
	text = strings.Trim(text, "{}")
	text = strings.TrimPrefix(strings.TrimSpace(text), `"text":`)
	text = strings.TrimSpace(text)
	text = strings.Trim(text, `"`)

	return text
}

func trimResponseForCliWhisper(config Config, text string) string {
	lines := strings.Split(text, "\n")

	debug(config, fmt.Sprintf("text : %s", text))
	processedLines := []string{}
	for _, line := range lines {
		if bracketIndex := strings.Index(line, "]"); bracketIndex != -1 {
			trimmed := strings.TrimSpace(line[bracketIndex+1:])
			if trimmed != "" {
				processedLines = append(processedLines, trimmed)
			}
		}
	}
	result := strings.Join(processedLines, " ")
	debug(config, fmt.Sprintf("Final text: %s", result))
	return result
}

func trimResponseForLocalWhisper(text string) string {
	text = strings.TrimSpace(text)
	text = strings.Replace(text, "\n", "", -1)

	// Extract just the text section after the last "text": field
	if lastIndex := strings.LastIndex(text, `"text":"`); lastIndex != -1 {
		text = text[lastIndex+8:] // Skip past the `"text":"` part
		if endIndex := strings.Index(text, `"`); endIndex != -1 {
			text = text[:endIndex]
		}
	}

	return strings.TrimSpace(text)
}

func processResponse(text string, config Config) (string, error) {
	debug(config, "Processing response")
	if config.PrependMods {
		prependText := "mods"
		if config.Local {
			debug(config, "Prepending local model: "+config.LocalLanguageModel)
			prependText = "mods -a local -m " + config.LocalLanguageModel
		}
		if config.ModsRole != "" {
			debug(config, "Prepending mods role: "+config.ModsRole)
			prependText += fmt.Sprintf(" --role %s", config.ModsRole)
		}
		text := fmt.Sprintf("'%s'", text)
		fmt.Println("Prepending mods command: ", prependText)
		modifiedText := fmt.Sprintf("%s %s", prependText, text)
		return modifiedText, nil
	}

	if config.ModsRole != "" && !config.PrependMods {
		debug(config, "Processing with mods role")
		fmt.Println("Processing with mods command: ", config.ModsRole)
		if !config.ModsAvailable {
			return "", fmt.Errorf("cannot process with mods: mods command not available")
		}

		var cmd *exec.Cmd
		if config.Local {
			debug(config, fmt.Sprintf("running local command: mods -a local -m %s --role %s %s", config.LocalLanguageModel, config.ModsRole, text))
			cmd = exec.Command("mods", "-a", "local", "-m", config.LocalLanguageModel, "--role", config.ModsRole, text)
		} else {
			debug(config, fmt.Sprintf("running command: mods --role %s %s", config.ModsRole, text))
			cmd = exec.Command("mods", "--role", config.ModsRole, text)
		}

		output, err := cmd.Output()
		if err != nil {
			return "", fmt.Errorf("mods processing failed: %v", err)
		}
		debug(config, "Mods processing complete, returning output")
		return string(output), nil
	}
	debug(config, "No processing needed, returning raw text")
	return text, nil
}

func copyToClipboard(text string) error {
	cmd := exec.Command("wl-copy")
	cmd.Stdin = bytes.NewBufferString(text)
	return cmd.Run()
}

func cleanup() {
	os.Remove(wavFile)
	os.Remove(mp3File)
}
