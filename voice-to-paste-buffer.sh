#!/bin/bash

if [ "$1" == "-m" ] && [ -n "$2" ]; then
    available_roles=$(mods --list-roles)
    if echo "$available_roles" | grep -q "$2"; then
        echo "Role '$2' is available."
    else
        echo "Error: Role '$2' is not available. Please choose from the following roles:"
        echo "$available_roles"
        exit 6
    fi
fi

# If recording.wav or recording.mp3 exists, warn the user and ask for confirmation
if [ -f recording.wav ] || [ -f recording.mp3 ]; then
  echo -e "\e[1;31mWARNING: recording exists, will use currently found audio for transcription. If yes; type y, if no, type n\e[0m"
  read -r response
  if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then
    echo "Using existing audio file for transcription."
  else
    rm recording.wav recording.mp3
  fi
else
  # If OPENAI_API_KEY is not set, warn the user and quit
  if [ -z "$OPENAI_API_KEY" ]; then
      echo "OPENAI_API_KEY is not set. Please set it and run the script again."
      exit 3
  fi

  # If arecord or lame is not installed, warn the user and quit
  if ! command -v arecord &> /dev/null; then
      echo "arecord is not installed. Please install it and run the script again."
      exit 4
  fi

  if ! command -v lame &> /dev/null; then
      echo "lame is not installed. Please install it and run the script again."
      exit 5
  fi

  echo "Recording audio..."
  echo "Press Ctrl+C to stop recording"
  arecord -f cd recording.wav

  lame recording.wav recording.mp3
fi

response=$(curl https://api.openai.com/v1/audio/transcriptions \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -H "Content-Type: multipart/form-data" \
  -F file="@recording.mp3" \
  -F model="whisper-1")

  if [ "$1" == "-m" ] && [ -n "$2" ]; then
    echo "-m passed with argument, will run through mods with specified role"
    echo "$response" | tr -d '{}' | cut -d ':' -f 2 | cut -c 3- | rev | cut -c 3- | rev | tr -d '\n' | tr -d "'" | xargs -I {} mods --role "$2" {} | wl-copy
else
  echo "$response" | tr -d '{}' | cut -d ':' -f 2 | cut -c 3- | rev | cut -c 3- | rev | tr -d '\n' | wl-copy
fi

# $* represents all command-line arguments as a single string
# *"--debug"* is a pattern that matches any string containing "--debug"
# In short, this searches for "--debug" in the command-line arguments
if [[ "$*" != *"--debug"* ]]; then
  rm recording.wav
  rm recording.mp3
fi

echo "done"
