#!/bin/bash

# If recording.wav or recording.mp3 exists, warn the user and quit
if [ -f recording.wav ]; then
  echo "recording.wav already exists. Please delete it and run the script again."
    if [ -f recording.mp3 ]; then
        echo "recording.mp3 already exists. Please delete it and run the script again."
    fi
  exit 1
fi

if [ -f recording.mp3 ]; then
    echo "recording.mp3 already exists. Please delete it and run the script again."
    exit 2
fi

# If OPENAI_API_KEY is not set, warn the user and quit
if [ -z "$OPENAI_API_KEY" ]; then
    echo "OPENAI_API_KEY is not set. Please set it and run the script again."
    exit 3
fi

# If arecord or lame is not installed, warn the user and quit
if ! command -v arecord &> /dev/null; then
    echo "arecord is not installed. Please install it and run the script again."
    exit 4
fi

if ! command -v lame &> /dev/null; then
    echo "lame is not installed. Please install it and run the script again."
    exit 5
fi


echo "Recording audio..."
echo "Press Ctrl+C to stop recording"
arecord -f cd recording.wav

lame recording.wav recording.mp3

response=$(curl https://api.openai.com/v1/audio/transcriptions \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -H "Content-Type: multipart/form-data" \
  -F file="@recording.mp3" \
  -F model="whisper-1")

echo $response | tr -d '{}' | cut -d ':' -f 2 | tr -d "'" | cut -c 3- | rev | cut -c 3- | rev | { read p; echo "mods '$p'"; } | tr -d '\n' | wl-copy

rm recording.wav
rm recording.mp3

echo "done"

