# Voice recording to pasate buffer using wayland

This is a very simple voice to paste buffer script, and only works on wayland, but can be modified eaily

most of the script checks to ensure it wont clobber a file, and makes surey ou have the dependencies

uses:
  arecord - to record audio
  lame - to convert the audio to mp3 (Saves 10x space)
  openai api using curl (you need an api key, and have it as an environment variable as OPENAI_API_KEY)
  wl-copy - to copy the text to the paste buffer (In wayland, so that ctrl+v works, instead of middle click which is what xclip does)
